package com.example.hw9

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.graphics.*
import android.widget.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    private lateinit var image: ImageView
    private lateinit var button: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        image = findViewById(R.id.imViewer)
        //начальное изображение
        image.setBackgroundResource(R.drawable.no_image)
        button = findViewById(R.id.btChanger)
        //список возможных изображений
        val picList = listOf(R.drawable.art_piece, R.drawable.city, R.drawable.girl, R.drawable.gothic, R.drawable.planet)
        //меняем стартовую картинку на  случайную картинку из списка и цвет кнопки на случайный
        button.setOnClickListener {
            val rand = Random.nextInt(picList.size)
            image.setBackgroundResource(picList[rand])
            val randomColor = Color.rgb(Random.nextInt(255), Random.nextInt(255), Random.nextInt(255))
            button.setBackgroundColor(randomColor)
        }
    }
}
